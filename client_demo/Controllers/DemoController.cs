﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using client_demo.Models;

namespace client_demo.Controllers
{
    public class DemoController : Controller
    {
       
        public ActionResult Index()
        {
            DemoClient demoCLient = new DemoClient();
            ViewBag.result1 = demoCLient.helloworld().Result;
            ViewBag.result2 = demoCLient.helloworldhtml().Result;
            ViewBag.result3 = demoCLient.find().Result;
            ViewBag.result4 = demoCLient.findall().Result;


            return View();
        }
    }
}