﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using server_demo.Models;

namespace client_demo.Models
{
    public class DemoClient
    {
        String BASE_URL = "http://localhost:52706/api/demo/";
        
        public Task<String> helloworld()
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(BASE_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                HttpResponseMessage response = client.GetAsync("helloworld").Result;
                return response.Content.ReadAsStringAsync();
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public Task<String> helloworldhtml()
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(BASE_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/html"));
                HttpResponseMessage response = client.GetAsync("helloworldhtml").Result;
                return response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Task<Student> find()
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(BASE_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("find").Result;
                return response.Content.ReadAsAsync<Student>();
               }
            catch
            {
                return null;
            }
        }

        public Task<List<Student>> findall()
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(BASE_URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("findall").Result;
                return response.Content.ReadAsAsync<List<Student>>();
            }
            catch
            {
                return null;
            }
        }
    }
}