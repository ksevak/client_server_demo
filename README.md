
                                            READ ME

1) Project Description
    Tool : Visual Studio 2017
    Framework : .NET 4.6.1
    Application Type :
    Server Side : ASP.NET Web Apllication > WEB API
    Client Side : ASP.NET Web Application > MVC

 2) Server Side Url(s)
    Main Task : http://localhost:52706/api/demo/findall [Object with multiple Data]
    Extra : http://localhost:52706/api/demo/helloworld [Plain Text]
            http://localhost:52706/api/demo/helloworldhtml [HTML Content]
            http://localhost:52706/api/demo/find [Object with Single Data]                           
    Client Side Url
            http://localhost:54840/demo/index
    
3) Lesson Learned
    Understanding of WEB API & MVC Framework
    When to use WEB API and when to use WCF
    What is REST API
    Managing References of Application and Datetime Object 
    Rendering HTML from ViewBag
    Code Debugging in Visual Studio


4) References :
    https://docs.microsoft.com/en-us/aspnet/web-api/overview/older-versions/build-restful-apis-with-aspnet-web-api
    https://www.youtube.com/watch?v=G3k6QeJ4vPQ
    https://youtu.be/6qwuFQDB2jU
