﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server_demo.Models
{
    public class StudentModel
    {
        public Student find()
        {
            return new Student
            {
                FirstName = "Krunal",
                LastName = "Sevak",
                BirthDate = new DateTime(1993, 09, 15).ToShortDateString(),
                IsNewStudent = false

            };
        }
        public List<Student> findall()
        {
            return new List<Student>()
            {
                new Student
                {
                    FirstName = "Krunal",
                    LastName = "Sevak",
                    BirthDate = new DateTime(1993, 09, 15).ToShortDateString(),
                    IsNewStudent = true
                },
                new Student
                {
                    FirstName = "Ishan",
                    LastName = "Trivedi",
                    BirthDate = new DateTime(1993, 07, 14).ToShortDateString(),
                    IsNewStudent = true
                },
                new Student
                {
                    FirstName = "Shrey",
                    LastName = "Shah",
                    BirthDate = new DateTime(1993, 08, 31).ToShortDateString(),
                    IsNewStudent = false
                }
            };
        }
    }

}