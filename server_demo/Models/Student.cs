﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server_demo.Models
{
    public class Student
    {
        public String FirstName
        {
            get;
            set;
        }
        public String LastName
        {
            get;
            set;
        }
        
        public String BirthDate
        {
            get;
            set;
        }

        public Boolean IsNewStudent
        {
            get;
            set;
        }
    }
}