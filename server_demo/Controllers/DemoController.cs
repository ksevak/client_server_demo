﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using server_demo.Models;

namespace server_demo.Controllers
{
    [RoutePrefix("api/demo")]
    public class DemoController : ApiController
    {
        [HttpGet]
        [Route("helloworld")]
        public HttpResponseMessage helloworld()
        {
            var response = new HttpResponseMessage();
            response.Content = new StringContent("Hello World");
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/plain");
            return response;

        }

        [HttpGet]
        [Route("helloworldhtml")]
        public HttpResponseMessage helloworldhtml()
        {
            var response = new HttpResponseMessage();
            response.Content = new StringContent("<h1>Hello World</h1>");
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/html");
            return response;

        }

        [HttpGet]
        [Route("find")]
        public HttpResponseMessage find()
        {
            StudentModel studentmodel = new StudentModel();
            var response = new HttpResponseMessage();
            response.Content = new StringContent(JsonConvert.SerializeObject(studentmodel.find()));
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            return response;

        }

        [HttpGet]
        [Route("findall")]
        public HttpResponseMessage findall()
        {
            StudentModel studentmodel = new StudentModel();
            var response = new HttpResponseMessage();
            response.Content = new StringContent(JsonConvert.SerializeObject(studentmodel.findall()));
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            return response;

        }
    }
}
